//
//  ColorScheme.swift
//  SymptomChecker
//
//  Created by Max Tharr on 22.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation
import SwiftUI

let orange = Color(hex: 0xF58A6A)
let darkBLue = Color(hex: 0x171954)
let lightGray = Color(hex: 0xDFECED)
let white = Color(hex: 0xFFFFFA)
