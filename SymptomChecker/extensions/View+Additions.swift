//
//  View+Additions.swift
//  SymptomChecker
//
//  Created by Max Tharr on 20.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

extension Color {
    init(hex: Int, alpha: Double = 1) {
        let components = (
            R: Double((hex >> 16) & 0xff) / 255,
            G: Double((hex >> 08) & 0xff) / 255,
            B: Double((hex >> 00) & 0xff) / 255
        )
        self.init(
            .sRGB,
            red: components.R,
            green: components.G,
            blue: components.B,
            opacity: alpha
        )
    }
}

extension View {
    func fancyStyle(active: Bool = true) -> some View {
        self
            .padding(10)
            .background(active ? Color.blue : Color.gray)
            .foregroundColor(.white)
            
            .cornerRadius(10)
            .shadow(color: (active ? Color.blue : Color.gray).opacity(0.5), radius: 4, x: 2, y: 2)
    }
    
    func orangeButtonStyle(active: Bool = true) -> some View {
        self
            .padding(20)
            .background(active ? orange : lightGray)
            .foregroundColor(active ? lightGray : orange)
            
            .cornerRadius(20)
            .shadow(color: (active ? orange : lightGray).opacity(0.5), radius: 4, x: 2, y: 2)
    }
    
    func blueButtonStyle(active: Bool = true) -> some View {
          self
              .padding(20)
              .background(active ? darkBLue : lightGray)
              .foregroundColor(active ? lightGray : darkBLue)
              
              .cornerRadius(20)
              .shadow(color: (active ? darkBLue : lightGray).opacity(0.5), radius: 4, x: 2, y: 2)
      }
    
    func orangeStyle(active: Bool = true) -> some View {
        self
            .foregroundColor(orange)
            
    }
    
    func blueStyle(active: Bool = true) -> some View {
        self
            .foregroundColor(darkBLue)
    }
    
    func whiteStyle(active: Bool = true) -> some View {
        self
            .foregroundColor(lightGray)
    }
    
    func materialTile() -> some View {
        self
            .padding()
            .cornerRadius(20)
            .shadow(radius: 8)
    }
    
    func graphButton(color: GradientColor, active: Bool) -> some View {
        self
        .padding(4)
            .background(active ? orange : lightGray)
            .foregroundColor(active ? white : darkBLue)
        .cornerRadius(10)
    }
}


struct TestView_Previews: PreviewProvider {
    static var previews: some View {
        Text("Test")
            .graphButton(color: .blu, active: false)
    }
}

