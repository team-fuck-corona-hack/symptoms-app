//
//  Date+Additions.swift
//  SymptomChecker
//
//  Created by Max Tharr on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation


extension Date {
    
    func weekDay() -> String {
        let index = Calendar.current.component(.weekday, from: self)
        return DateFormatter().shortWeekdaySymbols[index-1]
    }
    
    func dayNumber() -> String {
        String(Calendar.current.component(.day, from: self))
    }
}
