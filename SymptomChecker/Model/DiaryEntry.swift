//
//  Record.swift
//  SymptomChecker
//
//  Created by Max Tharr on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation

struct DiaryEntry: Identifiable {
    let id = UUID()
    let date: Date
    var isShown = true
    let fever: Double
    let couching: Double
    let headache: Double
    let throatache: Double
    let nausea: Double
    let bodyAche: Double
    let blockedNose: Double
    let shortBreath: Double
    
    static let previewDiaryEntries = [
        DiaryEntry(date: Date(), fever: 37, couching: 2, headache: 2, throatache: 0, nausea: 1, bodyAche: 2, blockedNose: 4, shortBreath: 3),
        DiaryEntry(date: Calendar.current.date(byAdding: .day, value: -1, to: Date())!, fever: 37, couching: 2, headache: 1, throatache: 0, nausea: 1, bodyAche: 3, blockedNose: 4, shortBreath: 1),
        DiaryEntry(date: Calendar.current.date(byAdding: .day, value: -2, to: Date())!, fever: 38, couching: 5, headache: 2, throatache: 1, nausea: 0, bodyAche: 3, blockedNose: 1, shortBreath: 3),
        DiaryEntry(date: Calendar.current.date(byAdding: .day, value: -3, to: Date())!, fever: 38, couching: 4, headache: 4, throatache: 1, nausea: 1, bodyAche: 4, blockedNose: 4, shortBreath: 2),
        DiaryEntry(date: Calendar.current.date(byAdding: .day, value: -4, to: Date())!, fever: 41, couching: 4, headache: 5, throatache: 4, nausea: 2, bodyAche: 2, blockedNose: 2, shortBreath: 3),
        DiaryEntry(date: Calendar.current.date(byAdding: .day, value: -5, to: Date())!, fever: 40, couching: 5, headache: 3, throatache: 5, nausea: 1, bodyAche: 3, blockedNose: 3, shortBreath: 3),
        DiaryEntry(date: Calendar.current.date(byAdding: .day, value: -6, to: Date())!, fever: 36, couching: 2, headache: 2, throatache: 3, nausea: 0, bodyAche: 2, blockedNose: 4, shortBreath: 4)
    ]
}



