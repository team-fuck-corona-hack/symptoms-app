//
//  Answer.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation

class Answer {
    var id: String
    var result: Any
    var answerType: AnswerType
    init(id: String, result: Any, answerType: AnswerType) {
        self.id = id
        self.result = result
        self.answerType = answerType
    }
}
