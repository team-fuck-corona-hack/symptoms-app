//
//  ContentView.swift
//  SymptomChecker
//
//  Created by Max Tharr on 20.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State
    var mazeCounter = 0
    
    @EnvironmentObject
    var loginState : LoginState
	
	@State
	var showModal = false
	
	@State
	var showInputModal = false
	
	init() {
		
		UITabBar.appearance().barTintColor = UIColor(red: 255 / 255, green: 255 / 255, blue: 250 / 255, alpha: 1.0)
		UITabBar.appearance().unselectedItemTintColor = UIColor(red: 223 / 255, green: 236 / 255, blue: 237 / 255, alpha: 1.0)
		UITabBar.appearance().tintColor = UIColor(red: 245 / 255, green: 138 / 255, blue: 106 / 255, alpha: 1.0)
    }
    
    var body: some View {
        
        VStack {
            if loginState.isLoggedIn {
                TabView(selection: $mazeCounter) {
                    MainView()
                        .tabItem { Image(systemName: "square.grid.2x2") }
                        .tag(0)
                    AlexTestView(title: "dada", placeholderText: "dada")
						.tabItem { Image(systemName: "calendar") }
                        .tag(1)
                    	InputView()
						.onAppear {
							DispatchQueue.main.async {
								self.showModal = true
							}
						}
                        .tabItem { Image(systemName: "plus.circle.fill").font(.system(size: 30)) }
                        .tag(2)
                   	InfoListView()
                        .tabItem { Image(systemName: "info.circle") }
                        .tag(3)
                    MazeTestView(counter: $mazeCounter)
						.tabItem { Image(systemName: "slider.horizontal.3") }
                        .tag(4)
				}.accentColor(Color(hex: 0xF58A6A))
            } else {
                WelcomeView()
            }
  
        }.environment(\.font, Font.system(.body, design: .rounded))
		.sheet(isPresented: self.$showModal) {
            NavigationView {
				QuestionnaireView(questionnaire: Questionnaire(questions:[
					NumericQuestion(id: "generalwellbeing", text: "Wie fühlst du dich heute?", answerType: AnswerType.string),
					NumericQuestion(id: "headache", text: "Wie würdest du deine Kopfschmerzen beschreiben?", answerType: AnswerType.string),
					ChoiceQuestion(id: "contacts", text: "Bist du schneller außer Atem als sonst?", answerType: AnswerType.string, choices: ["Ja", "Nein"]),
					ChoiceQuestion(id: "firstName", text: "Mit wievielen Leuten hattest du gestern Kontakt?", answerType: AnswerType.string, choices: ["Mit niemandem", "1-2 Personen", "3-4 Personen", ">5 Personen"]),
					TextQuestion(id: "anmerkungen", text: "Hast du noch weitere Anmerkungen?", answerType: AnswerType.string),
				]), submitCallback: { (parameters) -> Void in
					self.showModal = false;
					self.showInputModal = true;
				
					return;
				})
			}.accentColor(Color(hex: 0xF58A6A))
        }
    }
}

class LoginState: ObservableObject {
    @Published var isLoggedIn: Bool = false
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
