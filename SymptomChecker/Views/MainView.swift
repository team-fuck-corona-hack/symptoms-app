//
//  MainView.swift
//  SymptomChecker
//
//  Created by Max Tharr on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI
import ASCollectionView

struct MainView: View {
    @ObservedObject
    var viewModel = MainViewModel()
	
	@State
	var showModal = true
	
	@State
	var showOtherInputModal = false
	    /*
    var body: some View {
        
        VStack(spacing: 40) {
			
			Button("Show Modal") {
			   // 2.
			   self.showModal.toggle()
			// 3.
			}.sheet(isPresented: $showModal) {
				NavigationView {
				QuestionnaireView(questionnaire: Questionnaire(questions:[
					NumericQuestion(id: "generalwellbeing", text: "Wie fühlst du dich heute?", answerType: AnswerType.string),
					NumericQuestion(id: "headache", text: "Wie würdest du deine Kopfschmerzen beschreiben?", answerType: AnswerType.string),
					ChoiceQuestion(id: "contacts", text: "Bist du schneller außer Atem als sonst?", answerType: AnswerType.string, choices: ["Ja", "Nein"]),
					ChoiceQuestion(id: "firstName", text: "Mit wievielen Leuten hattest du gestern Kontakt?", answerType: AnswerType.string, choices: ["Mit niemandem", "1-2 Personen", "3-4 Personen", ">5 Personen"]),
					TextQuestion(id: "anmerkungen", text: "Hast du noch weitere Anmerkungen?", answerType: AnswerType.string),
				]), submitCallback: { (parameters) -> Void in
					self.showModal = false;
					
					self.sheet(isPresented: .constant(true)) {
						InputView()
					}
					
					return;
				}).foregroundColor(Color(hex: 0x171954))
				}
			 }
            HStack(spacing: 35) {
                ForEach(viewModel.dates) {
                    diaryEntry in
                    VStack(spacing: 5) {
                        Text(diaryEntry.date.weekDay()).blueStyle().font(.subheadline)
                        Text(diaryEntry.date.dayNumber())
                            .blueStyle().font(.headline)
                        Circle().foregroundColor(Color(hex: 0xF58A6A)).frame(width: 20, height: 20)
*/

    
    var body: some View {
        
        VStack(spacing: 50) {
            VStack(alignment: .leading, spacing: 0) {
                Text("März 2020").blueStyle().padding(.leading, 10).padding(.bottom, 10).font(.headline)
                HStack(spacing: 33) {
                    ForEach(viewModel.dates) {
                        diaryEntry in
                        VStack(spacing: 5) {
                            Text(diaryEntry.date.weekDay()).blueStyle().font(.subheadline)
                            Text(diaryEntry.date.dayNumber())
                                .blueStyle().font(.headline)
                            Circle().foregroundColor(orange).frame(width: 20, height: 20)
                        }
                    }
                }.frame(width: 400)
                ZStack {
                   // Sorted, damit der aktive immer vorne ist
                    ForEach(viewModel.graphData.sorted(by: {
                        a,b in a.isActive == false})) {
                        graph in
                        MultiLineView(
                            data: graph.dataRow,
                            gradient: graph.isActive ? GradientColor.orange : GradientColor.blu,
                            min: graph.min,
                            max: graph.max)
                    }
                }
                .padding()
                .frame(width: 400, height: 240)
                ASCollectionView(
                    section:
                    ASCollectionViewSection(id: 0, data: viewModel.graphData)
                    { entry, _ in
                    Button(action: {self.toggleGraph(entry: entry)}) {
                        Text(entry.legend)
                            .graphButton(color: entry.gradient, active: entry.isActive)
                            .fixedSize()
                        }
                    }).layout {
                        let fl = AlignedFlowLayout()
                        fl.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
                        return fl
                }.padding()
            }
            
            VStack(alignment: .leading ,spacing: 5) {
                Text("Wie geht es dir heute?").font(.title)
            }
            .multilineTextAlignment(.leading)
            .padding()
            .frame(width: 400)
            .foregroundColor(darkBLue)
            Spacer()
		}
    }
    
    func toggleGraph(entry: GraphData) {
        let index = viewModel.graphData.firstIndex(where: {$0 == entry})
        if let index = index {
            withAnimation {
                viewModel.toggleIndex(index: index)
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
