//
//  ViewWithModelExample.swift
//  SymptomChecker
//
//  Created by Max Tharr on 20.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct ViewWithModelExample: View {
    @ObservedObject
    var viewModel = ViewWithModelExampleViewModel()
    
    var body: some View {
        VStack {
            Text(viewModel.publishedValue)
            TextField("Hier schreiben", text: $viewModel.publishedValue)
        }
    }
}

struct ViewWithModelExample_Previews: PreviewProvider {
    static var previews: some View {
        ViewWithModelExample()
    }
}
