//
//  File.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct QuestionView: View {
    @State
    var resultText: String = ""
    var submitCallback: (Questionnaire) -> Void
    private var titleText: String
    private var question: Question
    private var questionnaire: Questionnaire
    private var index: Int
    @State private var action: Int? = 0
    @EnvironmentObject var loginState : LoginState
    
    
    init(questionnaire: Questionnaire, index: Int, submitCallback: @escaping (Questionnaire) -> Void) {
        self.questionnaire = questionnaire
        self.question = questionnaire.questions[index]
        self.titleText = self.question.text
        self.submitCallback = submitCallback
        self.index = index
    }
    
    var body: some View {
        VStack(spacing: 15) {
            Text(self.titleText)
                .blueStyle()
                .padding(.bottom, 30)
                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
            
            if question is TextQuestion {
                UnderlinedTextField(placeholder: "", resultText: $resultText, isSecure: false).blueStyle()
            } else if question is PasswordQuestion {
                UnderlinedTextField(placeholder: "", resultText: $resultText, isSecure: true)
            } else if question is NumericQuestion {
                NumericQuestionView(question: question, resultText: $resultText)
            } else if question is ChoiceQuestion {
                NavigationLink(destination: QuestionView(questionnaire: self.questionnaire, index: self.index + 1, submitCallback: self.submitCallback), tag: 1, selection: $action) {
                    ChoiceQuestionView(question: question, resultText: $resultText, submitCallback: self.onButtonPress, action: $action)
                }.onDisappear() {
                    self.onButtonPress()
                }
                
            }
            
            if !(question is ChoiceQuestion) {
                if self.resultText != "" {
                    if (self.index + 1 < self.questionnaire.questions.count) {
                        NavigationLink(destination: QuestionView(questionnaire: self.questionnaire, index: self.index + 1, submitCallback: self.submitCallback)) {
                            Text("Weiter").orangeButtonStyle()
                        }.onDisappear() {
                            self.onButtonPress()
                        }
                    }
                    else {
                        Button(action: {
                            self.onButtonPress()
                            self.onSubmitButton(questionnaire: self.questionnaire)
                        })
                        {
                            Text("Fertig").orangeButtonStyle()
                        }
                    }
                } else {
                    if (self.index + 1 < self.questionnaire.questions.count) {
                        Text("Weiter").orangeButtonStyle(active: false)
                    }
                    else {
                        Text("Fertig").orangeButtonStyle(active: false)
                    }
                }
            }
        }
    }
    
    func onButtonPress(){
        print("OnDisappear")
        let answer = Answer(id: self.question.id, result: self.resultText, answerType: self.question.answerType)
        self.questionnaire.answers[self.question.id] = answer
        print("Recorded answer " + (answer.result as! String) + " for question " + self.question.text)
        print(self.questionnaire.answers.keys)
        print(self.questionnaire.answers.values)
    }
    
    func onSubmitButton(questionnaire: Questionnaire) {
        self.submitCallback(questionnaire)
    }
}

struct QuestionView_Previews: PreviewProvider {
    static var previews: some View {
        QuestionView(questionnaire: Questionnaire(questions: [
        ChoiceQuestion(id: "gender", text: "Was ist dein Geschlecht?", answerType: AnswerType.string, choices: ["MALE", "FEMALE", "OTHER"]),
        TextQuestion(id: "1", text: "Wie heißt du?", answerType: AnswerType.string)]), index: 0, submitCallback: { (parameters) -> Void in return })
    }
}
