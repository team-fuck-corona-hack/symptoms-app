//
//  MainViewModel.swift
//  SymptomChecker
//
//  Created by Max Tharr on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation
import Combine

class MainViewModel: ObservableObject {
    @Published
    var dates = DiaryEntry.previewDiaryEntries
    
    @Published
    var graphData: [GraphData] {
        didSet {
            print("was set")
        }
    }
    
    init() {
        let dates = DiaryEntry.previewDiaryEntries
        let fever = GraphData(gradient: .orngPink, dataRow: dates.map { $0.fever },legend:  "Fieber", min: 35.5, max: 42, isActive: true)
        let coughing = GraphData(gradient: .blu, dataRow: dates.map { $0.couching },legend: "Husten")
        let headache = GraphData(gradient: .green, dataRow: dates.map { $0.headache },legend: "Kopfschmerz")
        let throatache = GraphData(gradient: .prplPink, dataRow: dates.map { $0.throatache },legend: "Halschmerz")
        let nausea = GraphData(gradient: .blue, dataRow: dates.map { $0.nausea },legend: "Übelkeit")
        let bodyAche = GraphData(gradient: .bluPurpl, dataRow: dates.map { $0.bodyAche },legend: "Gliederschmerz")
        let blockedNose = GraphData(gradient: .orange, dataRow: dates.map { $0.blockedNose },legend: "verstopfte Nase")
        let shortBreath = GraphData(gradient: .prplNeon, dataRow: dates.map { $0.shortBreath },legend: "Kurzatmigkeit")
        graphData = [fever,coughing,headache,throatache, nausea, bodyAche, blockedNose, shortBreath]
    }
    
    func toggleIndex(index: Int) {
        print("Toggling \(index)")
        for i in 0..<graphData.count {
            graphData[i].isActive = false
        }
        graphData[index].isActive = true
    }
}

struct GraphData: Identifiable, Equatable {
    init(gradient: GradientColor, dataRow: [Double], legend: String, min: Double = -0.5, max: Double = 5.5, isActive: Bool = false) {
        self.gradient = gradient
        self.dataRow = dataRow
        self.legend = legend
        self.min = min
        self.max = max
        self.isActive = isActive
    }
    
    let id = UUID()
    var isActive: Bool
    let gradient: GradientColor
    let dataRow: [Double]
    let legend: String
    var min: Double
    var max: Double
    
    
    static func == (lhs: GraphData, rhs: GraphData) -> Bool {
        return lhs.id == rhs.id
    }
}

