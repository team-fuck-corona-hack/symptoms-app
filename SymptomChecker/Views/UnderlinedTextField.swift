//
//  UnderlinedTextField.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//

import Foundation
import SwiftUI

struct UnderlinedTextField: View {
    var resultText: Binding<String>
    private var placeholder = ""
    private let lineThickness = CGFloat(2.0)
    private var isSecure = false
    
    init(placeholder: String, resultText: Binding<String>, isSecure: Bool = false) {
        self.placeholder = placeholder
        self.resultText = resultText
        self.isSecure = isSecure
    }
    
    var body: some View {
        VStack {
            if (isSecure) {
                SecureField(placeholder, text: resultText).disableAutocorrection(true).autocapitalization(.none)
            }
            else {
                TextField(placeholder, text: resultText).disableAutocorrection(true).autocapitalization(.none)
            }
            
            HorizontalLine(color: .gray)
        }.padding(.bottom, lineThickness)
            .multilineTextAlignment(.center)
    }
}


//struct TextFieldWithBottomLine_Previews: PreviewProvider {
//   static var previews: some View {
//UnderlinedTextField(placeholder: "My placeholder", resultText: )
// }
//}

struct HorizontalLineShape: Shape {
    
    func path(in rect: CGRect) -> Path {
        
        let fill = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        var path = Path()
        path.addRoundedRect(in: fill, cornerSize: CGSize(width: 2, height: 2))
        
        return path
    }
}

struct HorizontalLine: View {
    private var color: Color? = nil
    private var height: CGFloat = 1.0
    
    init(color: Color, height: CGFloat = 1.0) {
        self.color = color
        self.height = height
    }
    
    var body: some View {
        HorizontalLineShape().fill(self.color!).frame(minWidth: 0, maxWidth: 250, minHeight: height, maxHeight: height)
    }
}

struct HorizontalLine_Previews: PreviewProvider {
    static var previews: some View {
        HorizontalLine(color: .gray)
    }
}
