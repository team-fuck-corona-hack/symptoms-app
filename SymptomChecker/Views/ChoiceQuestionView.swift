//
//  File.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct ChoiceQuestionView: View {
    
    var question: ChoiceQuestion
    var resultText: Binding<String>
    var submitCallback: () -> Void
    var action: Binding<Int?>
    
  
    @State var selection = ""
    init(question: Question, resultText: Binding<String>, submitCallback: @escaping () -> Void, action: Binding<Int?>) {
        self.question = question as! ChoiceQuestion
        self.resultText = resultText
        self.submitCallback = submitCallback
        self.action = action
    }
    var body: some View {
        
        VStack(alignment: .center, spacing: 20) {
            ForEach(self.question.choices, id: \.self) { element in
                Text("\(getDisplayName(inStr: element))").orangeButtonStyle(active: element == self.selection)
                .scaleEffect(element == self.selection ? 1.2 : 1).animation(.easeInOut(duration: 0.3))
                    .onTapGesture {
                        self.resultText.wrappedValue = element
                        self.selection = element
                        self.submitCallback()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                            self.action.wrappedValue = 1
                        }
                       
                }
                
                
            }
        }.padding(.bottom, 20.0)
    }
}

struct ChoiceQuestionView_Previews: PreviewProvider {
    static var previews: some View {
        QuestionView(questionnaire: Questionnaire(questions: [ChoiceQuestion(id: "1", text: "Wie alt bist du?", answerType: .string,choices: ["hi","yaaaaaao","he"])]), index: 0, submitCallback: { (parameters) -> Void in return  })
        
    }
}
