//
//  ViewWithModelExampleViewModel.swift
//  SymptomChecker
//
//  Created by Max Tharr on 20.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation
import Combine

class ViewWithModelExampleViewModel: ObservableObject {
    @Published
    var publishedValue = ""
}
