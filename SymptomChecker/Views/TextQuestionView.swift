//
//  File.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct TextQuestionView: View {

    var question: Question
    var resultText: Binding<String>

    init(question: Question, resultText: Binding<String>) {
        self.question = question
        self.resultText = resultText
    }
    var body: some View {
        UnderlinedTextField(placeholder: "", resultText: resultText)
    }
}
