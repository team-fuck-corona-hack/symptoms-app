//
//  InputView.swift
//  SymptomChecker
//
//  Created by Matthias Lehner on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct InputView: View {
	
	@Environment(\.presentationMode)
	var presentationMode
	
	@State
	var numberOfEntries = 89348
	
    var body: some View {
			VStack {
			
				HStack{
					Spacer()
				}
			
				Spacer()
			
				Text("Danke für deinen Input")
					.foregroundColor(Color(hex: 0x171954))
					.font(.system(size: 32))
				
				Text("Unser System wird schlauer")
					.foregroundColor(Color(hex: 0x171954))
			
				Spacer()
			
				ZStack {
					Circle()
						.fill(Color(hex: 0xFFFFFA))
					.frame(width: 300, height: 300)
				
					VStack {
						Text("\(numberOfEntries)")
							.foregroundColor(Color(hex: 0xF58A6A))
							.font(.system(size: 64))
						Text("Einträge")
							.foregroundColor(Color(hex: 0x171954))
					}
				}
				
				Spacer()
			
			}
			.background(Color(hex: 0xDFECED))
			.edgesIgnoringSafeArea(.all)
	}
}

struct InputView_Previews: PreviewProvider {
    static var previews: some View {
        InputView()
    }
}
