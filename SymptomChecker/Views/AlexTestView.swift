//
//  AlexTestView.swift
//  SymptomChecker
//
//  Created by Max Tharr on 20.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct AlexTestView: View {
    
    @State
    var resultText: String = "Enter your answer here"
    
    private var titleText: String
    private var placeholderText: String = ""
    
    init(title: String, placeholderText: String) {
        self.placeholderText = placeholderText
        self.titleText = title
    }
    
    var body: some View {
            VStack(spacing: 15) {
                Text(self.titleText)
                    .foregroundColor(.gray)
                    .padding(.bottom, 30)
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    
               // UnderlinedTextField(placeholder: placeholderText)

                Button(action: {
                    
                }) {
                    Text("Weiter").fancyStyle(active: true)
                }
            }
    }
    
}

struct AlexTestView_Previews: PreviewProvider {
    static var previews: some View {
        AlexTestView(title: "Wie heißt du?", placeholderText: "Name")
    }
}
