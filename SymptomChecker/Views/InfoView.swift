//
//  InfoView.swift
//  SymptomChecker
//
//  Created by Matthias Lehner on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct InfoView: View {
	
	@State
	var infoElement: InfoElement

    var body: some View {
		
		VStack(alignment: HorizontalAlignment.leading) {
			
			HStack {
				Spacer()
			}
			
			Text("\(infoElement.title)")
				.foregroundColor(Color(hex: 0x171954))
				.fontWeight(.bold)
				.padding(.top, 16)
				.padding(.leading, 16)
			
			Text("\(infoElement.description)")
				.foregroundColor(Color(hex: 0x171954))
				.padding(.top, 16)
				.padding(.leading, 16)
			
			Spacer()
		}
			.background(
				LinearGradient(
					gradient: Gradient(colors: [
						infoElement.gradientStartColor,
						infoElement.gradientEndColor
					]),
					startPoint: .topLeading,
					endPoint: .bottomTrailing
				)
			)
			.cornerRadius(16)
			//.shadow(radius: 5)
	}
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
		InfoView(infoElement: InfoElement(id: UUID(), title: "Title", description: "kdf dsf dfs fsd g sgf sdf gf gfds fdg gfs sdf gf sdf gf gsdf gfds fdgs dfg gf b sfdg wqe wr ge rsfdg wer", gradientStartColor: Color(red: 113 / 255, green: 178 / 255, blue: 128 / 255), gradientEndColor: Color(red: 19 / 255, green: 78 / 255, blue: 94 / 255)))
    }
}

