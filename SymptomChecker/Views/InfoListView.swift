//
//  Info Screen.swift
//  SymptomChecker
//
//  Created by Max Tharr on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI
import Combine

struct InfoListView: View {
    
    @ObservedObject
    var viewModel = StubViewModel()
    
    var body: some View {
		VStack{
			ZStack {
				VStack {
					HStack {
						Spacer()
						VStack {
							Text("Tipps & Tricks").foregroundColor(Color(hex: 0x171954)).padding(.top, 100).padding(.bottom, 150).font(.system(size: 32))
						}
						Spacer()
						}.background(Color(hex: 0xF58A6A))
					Spacer()
				}
				VStack(spacing: 16) {
					ForEach(viewModel.listElements) { element in
					InfoView(infoElement: element)
						.frame(height: 150)
					}
					Spacer()
				}
				.padding()
				.padding(.top, 195)
			}
		}
		.background(Color(hex: 0xFFFFFA))
		.edgesIgnoringSafeArea(.all)
    }
}

struct InfoListView_Preview: PreviewProvider {
    static var previews: some View {
        InfoListView()
    }
}

struct InfoElement: Identifiable {
	let id: UUID
	var title: String
	var description: String
	var gradientStartColor: Color
	var gradientEndColor: Color
}

class StubViewModel: ObservableObject {
		
    @Published
	var listElements: [InfoElement] = [
		InfoElement(id: UUID(), title: "So hilfst du mit", description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore.", gradientStartColor: Color(hex: 0xDFECED), gradientEndColor: Color(hex: 0xDFECED)),
		InfoElement(id: UUID(), title: "Weitere Quellen", description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.", gradientStartColor: Color(hex: 0xDFECED), gradientEndColor: Color(hex: 0xDFECED))
    ]

}
