//
//  ExampleList.swift
//  SymptomChecker
//
//  Created by Max Tharr on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI
import Combine

struct ExampleList: View {
    
    @ObservedObject
    var viewModel = ExampleViewModel()
    
    
    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.listElements, id: \.self) {
                    element in
                    NavigationLink(destination: AlexTestView(title: "title", placeholderText: "placeholder")) {
                        
                        Text(element)
                    }
                }
            }.navigationBarTitle("Übersicht")
        }
    }
}

struct ExampleList_Previews: PreviewProvider {
    static var previews: some View {
        ExampleList()
    }
}

class ExampleViewModel: ObservableObject {
    @Published
    var listElements = [
        "Hallo",
        "Test",
        "Bullshit"
    ]
    
    
}
