//
//  WelcomeView.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct WelcomeView: View {
    @EnvironmentObject var loginState : LoginState
    
    var body: some View {
        NavigationView {
            
            VStack {
                Image("CoronaIcon").padding(.bottom, 100).padding(.top, -200)
                VStack {
                    NavigationLink(destination: QuestionnaireView(questionnaire: Questionnaire(questions: [
                        TextQuestion(id: "userName", text: "Wie ist dein Username?", answerType: AnswerType.string),
                        PasswordQuestion(id: "password", text: "Wie ist dein Passwort?", answerType: AnswerType.string),
                    ]), submitCallback: { (questionnaire: Questionnaire) -> Void in
                        NetworkService.shared.login(username: questionnaire.answers["userName"]?.result as! String,
                                                    password: questionnaire.answers["password"]?.result as! String,
                                                    onCompletion: { (success: Bool, token: String?) in
                                                        print("onCompletion login")
                                                        print(success)
                                                        DispatchQueue.main.async {
                                                            self.loginState.isLoggedIn = success
                                                        }
                                                        NetworkService.shared.getQuestions(domain: .initial) { (success: Bool, questions: [Question]) in
                                                            print("getQuestions")
                                                            print(success)
                                                            print(questions)
                                                        }
                        })})) {
                            Text("Login").fixedSize()
                                .frame(width: 200, height: 0).orangeButtonStyle()
                    }.padding(.bottom, 10)
                    
                    NavigationLink(destination: QuestionnaireView(questionnaire: Questionnaire(questions: [
                        ChoiceQuestion(id: "gender", text: "Was ist dein Geschlecht?", answerType: AnswerType.string, choices: ["MALE", "FEMALE", "OTHER"]),
                        TextQuestion(id: "firstName", text: "Wie ist dein Vorname?", answerType: AnswerType.string),
                        TextQuestion(id: "lastName", text: "Wie ist dein Nachname?", answerType: AnswerType.string),
                        TextQuestion(id: "userName", text: "Wähle einen Nutzernamen", answerType: AnswerType.string),
                        PasswordQuestion(id: "password", text: "Wähle ein Passwort", answerType: AnswerType.string),
                        ChoiceQuestion(id: "ageRange", text: "Wie alt bist du?", answerType: AnswerType.string, choices: [AgeRange.youngerThan40.rawValue, AgeRange.between40And50.rawValue, AgeRange.between51And60.rawValue, AgeRange.between61And70.rawValue, AgeRange.between71And80.rawValue,AgeRange.above80.rawValue]),
                        TextQuestion(id: "zipcode", text: "Wie lautet deine Postleitzahl?", answerType: AnswerType.string),
                    ]), submitCallback: { (questionnaire: Questionnaire) -> Void in
                        NetworkService.shared.register(firstName: questionnaire.answers["firstName"]?.result as! String,
                                                       lastName: questionnaire.answers["lastName"]?.result as! String,
                                                       username: questionnaire.answers["userName"]?.result as! String,
                                                       password: questionnaire.answers["password"]?.result as! String,
                                                       gender: Gender(rawValue: questionnaire.answers["gender"]?.result as! String) ?? Gender.other,
                                                       ageRange: AgeRange(rawValue: questionnaire.answers["ageRange"]?.result as! String) ?? AgeRange.above80,
                                                       zipcode: questionnaire.answers["zipcode"]?.result as! String,
                                                       onCompletion: { (success: Bool) in
                                                        DispatchQueue.main.async {
                                                            self.loginState.isLoggedIn = success
                                                        }
                                                        
                                                        
                        })})) {
                            Text("Register").fixedSize()
                            .frame(width: 200, height: 0).orangeButtonStyle()
                    }.padding(.top, 10)
                
                }
            }
            
        }
        .padding(.bottom, 0)
    }
    
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
