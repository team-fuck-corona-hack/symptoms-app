//
//  RegistrationView.swift
//  SymptomChecker
//
//  Created by Max Tharr on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct QuestionnaireView: View {
    var model = QuestionnaireViewModel()
    @State var questionCounter: Int = 0
    var questionnaire: Questionnaire
    var submitCallback: (Questionnaire) -> Void
    init(questionnaire: Questionnaire, submitCallback: @escaping (Questionnaire) -> Void) {
        print("inited questionnaire view")
        self.questionnaire = questionnaire
        self.submitCallback = submitCallback
    }
    
    var body: some View {
        QuestionView(questionnaire: self.questionnaire, index: 0, submitCallback: self.submitCallback )
    }
}

class QuestionnaireViewModel: ObservableObject {
    var name: String = ""
    var email: String = ""
    var password: String = ""
}

struct QuestionnaireView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            QuestionnaireView(questionnaire: Questionnaire(questions: [
                NumericQuestion(id: "generalwellbeing", text: "Wie fühlst du dich heute?", answerType: AnswerType.string),
                NumericQuestion(id: "headache", text: "Wie würdest du deine Kopfschmerzen beschreiben?", answerType: AnswerType.string),
                ChoiceQuestion(id: "contacts", text: "Bist du schneller außer Atem als sonst?", answerType: AnswerType.string, choices: ["Ja", "Nein"]),
                ChoiceQuestion(id: "firstName", text: "Mit wievielen Leuten hattest du gestern Kontakt?", answerType: AnswerType.string, choices: ["Mit niemandem", "1-2 Personen", "3-4 Personen", ">5 Personen"]),
                TextQuestion(id: "anmerkungen", text: "Hast du noch weitere Anmerkungen?", answerType: AnswerType.string),
            ]), submitCallback: { (questionnaire: Questionnaire) -> Void in
                return
            })
            
        }
    }
}
