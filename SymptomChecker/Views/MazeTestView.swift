//
//  MazeTestView.swift
//  SymptomChecker
//
//  Created by Max Tharr on 20.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

struct MazeTestView: View {
    @Binding
    var counter: Int
    
    @State
    var secondCounter = 0
    
    var body: some View {
        ZStack {
            Color.gray.edgesIgnoringSafeArea(.all)
            VStack(alignment: .center) {
                if secondCounter.isMultiple(of: 2) {
                    Text("Hello, Maze! \(counter)").transition(.opacity)
                } else {
                    Image(systemName: "heart").transition(.opacity)
                }
                Button(action: {
                    withAnimation {
                        self.secondCounter += 1
                    }
                }) {
                    Text("Increase Counter")
                }
            }
            .frame(width: 300, height: 300, alignment: .center)
            .background(Color.white)
        .shadow(radius: 2)
        }
    }
}

struct MazeTestView_Previews: PreviewProvider {
    static var previews: some View {
        MazeTestView(counter: .constant(5))
    }
}
