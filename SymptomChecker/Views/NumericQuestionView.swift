//
//  File.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import SwiftUI

extension String {
    var double: Double {
        get { (self as NSString).doubleValue }
        set { self = String(Int(newValue)) }
    }
}


  
struct NumericQuestionView: View {

    var question: NumericQuestion
    var resultText: Binding<String>
    var showEmojis: Bool
    
    let emojiDict: [String: String] = [
        "0": "veryhappy",
        "1": "happy",
        "2": "ok",
        "3": "meh",
        "4": "unhappy",
        "5": "veryunhappy"
    ]

    init(question: Question, resultText: Binding<String>, showEmojis: Bool = true) {
        self.resultText = resultText
        self.question = question as! NumericQuestion
        self.showEmojis = showEmojis
        self.resultText.wrappedValue = "0"
        self.resultText.update()
    }
    
    var body: some View {
        VStack {
            if self.showEmojis {
                Image(self.emojiDict[self.resultText.wrappedValue] ?? "veryhappy").resizable()
                .frame(width: 62.0, height: 62.0).padding(.bottom, 20)
            }
            Slider(value: resultText.double, in: Double(question.minValue)...Double(question.maxValue))
            .orangeStyle()
                .padding(.horizontal)
                .accentColor(orange)//, 50.0)//, step: 1, onEditingChanged: onSliderChange, minimumValueLabel: 5, maximumValueLabel: 20, label: {})
            if !self.showEmojis {
                Text("\(resultText.wrappedValue)")
            }
        }
    }
    
}

struct NumericQuestionView_Previews: PreviewProvider {
    static var previews: some View {
		QuestionView(questionnaire: Questionnaire(questions: [NumericQuestion(id: "1", text: "Wie fühlst du dich?", answerType: AnswerType.string, minValue: 0, maxValue: 5)]), index: 0, submitCallback: { (parameters) -> Void in return })

    }
}
