//
//  NetworkService.swift
//  SymptomChecker
//
//  Created by Max Tharr on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation
import Combine

enum Gender: String {
	case male = "MALE"
	case female = "FEMALE"
	case other = "OTHER"
}

enum AgeRange: String {
	case youngerThan40 = "<40"
	case between40And50 = "40-50"
	case between51And60 = "50-60"
	case between61And70 = "60-70"
	case between71And80 = "70-80"
	case above80 = "80<"
}

enum Domain: String {
	case initial = "INITIAL"
	case dailyLog = "DAILY_LOG"
}

enum QuestionType: String {
	case selection = "SELECTION"
	case input = "INPUT"
	case other = "OTHER"
}

class NetworkService {
	
    static let shared = NetworkService()
	
    
    private let baseURL = URL(string: "https://answers.hackend-team-fuck-corona-hack.apps.moti.us/")!
	private var token: String?
    
    private init() {}
	
	/**
	Registers as new user.

	- Parameter firstName: The first name of the to be registered person
	- Parameter lastName: The last name of the to be registered person
	- Parameter username: The username of the to be registered person
	- Parameter password: The password of the to be registered person
	- Parameter gender: The gender of the to be registered person
	- Parameter ageRange: The age of the to be registered person
	- Parameter zipcode: The zipcode  of the to be registered person
	- Parameter onCompletion: A completion handler that receives a boolean indicating successfull or unsucessfull completion.
	*/
	func register(firstName: String, lastName: String, username: String, password: String, gender: Gender, ageRange: AgeRange, zipcode: String, onCompletion: @escaping (_ success: Bool) -> Void) {
		
        let url = baseURL.appendingPathComponent("users/register")
		
        var request = URLRequest(url: url)
				
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
		
        request.httpBody = try! JSONEncoder().encode([
			"first_name": firstName,
			"last_name": lastName,
			"username": username,
			"password": password,
			"gender": gender.rawValue,
			"age_range": ageRange.rawValue,
			"zipcode": zipcode
		])
        
        print([
            "first_name": firstName,
            "last_name": lastName,
            "username": username,
            "password": password,
            "gender": "\(gender.rawValue)",
            "age_range": "\(ageRange.rawValue)",
            "zipcode": zipcode
        ])
		
		let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
			
			guard let _ = data else {
				onCompletion(false)
				return
			}
			
			if error != nil {
				onCompletion(false)
				return
			}
			
			if (response as? HTTPURLResponse)?.statusCode != 201 {
				onCompletion(false)
				return
			}
			
			onCompletion(true)
		}
		
		task.resume()
    }
	
	/**
	Logs a user in.

	- Parameter username: The first name of the to be registered person
	- Parameter password: The last name of the to be registered person
	- Parameter onCompletion: A completion handler that receives a boolean indicating successfull or unsucessfull completion.
	*/
	func login(username: String, password: String, onCompletion: @escaping (_ success: Bool, _ token: String?) -> Void) {
		
        let url = baseURL.appendingPathComponent("users/login")
		
        var request = URLRequest(url: url)
		
        request.httpMethod = "POST"
		
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		
		request.httpBody = try! JSONEncoder().encode([
			"username": username,
			"password": password
		])
				
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
				
			guard let data = data else {
				onCompletion(false, nil)
				return
			}
						
			let json = try? JSONSerialization.jsonObject(with: data, options: [])
			
			if let response = json as? [String: Any] {
				if let token = response["access_token"] as? String {
					self.token = token
					onCompletion(true, token)
					return
				}
			}
			
			onCompletion(false, nil)
		}
			
		task.resume()
    }
	
	/**
	Logs the current user out of the system.
	*/
	func logout() {
		self.token = nil
    }
	
	/**
	Checks if currently a user is logged in.
	
	- Returns: true if any user is currently logged in, false otherwise
	*/
	func isLoggedIn() -> Bool {
		return self.token != nil
	}
	
	/**
	Get the token for the currently logged in user.
	
	- Returns: The token for the currently logged in user
	*/
	func getToken() -> String? {
		return self.token
	}
	
	/**
	Registers as new user.

	- Parameter domain: The domain for which you want to retrieve questions
	- Parameter onCompletion: A completion handler that receives a boolean indicating successfull or unsucessfull completion and an array of questions.
	*/
	func getQuestions(domain: Domain, onCompletion: @escaping (_ success: Bool, _ questions: [Question]) -> Void) {
		
        var url = baseURL.appendingPathComponent("questions")
				
		url = url.appending("domain", value: domain.rawValue)
								
        var request = URLRequest(url: url)
		
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.setValue("Bearer \(self.token ?? "")", forHTTPHeaderField: "Authorization")
		
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
							
			guard let data = data else {
				onCompletion(false, [])
				return
			}
						
			let json = try? JSONSerialization.jsonObject(with: data, options: [])
						
			if let response = json as? [[String: Any]] {
				
				var parsedEntries: [Question] = []
				
				for entry in response {
					
					let type: String = entry["type"] as? String ?? ""
										
					switch type {
					case QuestionType.selection.rawValue:
						if let id = entry["id"] as? Int {
							
							let stringId = String(id)
							
							if let text = entry["text"] as? String {
								if let answerType = entry["answer_type"] as? String {
								    if let choices = entry["choices"] as? [String] {
										let question = ChoiceQuestion(id: stringId, text: text, answerType: AnswerType(rawValue: answerType) ?? AnswerType.string, choices: choices)
									    parsedEntries.append(question)
									}
								}
							}
						}
					case QuestionType.input.rawValue:
						if let id = entry["id"] as? Int {
							
							let stringId = String(id)

							if let text = entry["text"] as? String {
								if let answerType = entry["answer_type"] as? String {
									let question = TextQuestion(id: stringId, text: text, answerType: AnswerType(rawValue: answerType) ?? AnswerType.string)
									parsedEntries.append(question)
								}
							}
						}
						
					default:
						print("QuestionType not implemented yet")
					}
				}
				
				onCompletion(true, parsedEntries)
				return
			}
			
			onCompletion(false, [])
		}
			
		task.resume()
    }
	
	/**
	Registers as new user.

	- Parameter domain: The domain for which you want to retrieve questions
	- Parameter onCompletion: A completion handler that receives a boolean indicating successfull or unsucessfull completion and an array of questions.
	*/
	func createAnswer(questionId: String, answer: String, type: AnswerType, onCompletion: @escaping (_ success: Bool) -> Void) {
		
        let url = baseURL.appendingPathComponent("answers")
						
        var request = URLRequest(url: url)
		
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.setValue("Bearer \(self.token ?? "")", forHTTPHeaderField: "Authorization")
		
		let body: [String: Any] = [
			"question_id": Int(questionId) ?? 0,
			"response": answer,
			"type": type.rawValue
		]
				
		request.httpBody = try! JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
						
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
					   
			guard let _ = data else {
				onCompletion(false)
				return
			}
			
			if error != nil {
				onCompletion(false)
				return
			}
						
			if (response as? HTTPURLResponse)?.statusCode != 201 {
				onCompletion(false)
				return
			}
			
			onCompletion(true)
		}
				   
		task.resume()
    }
}
