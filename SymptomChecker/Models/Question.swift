//
//  InputScreen.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//
import Foundation
enum AnswerType: String {
    case string = "STRING"
    case date = "DATE"
}
class Question {
    var id: String
    var text: String
    var answer: String
    var answerType: AnswerType
	init(id: String, text: String, answerType: AnswerType) {
		self.id = id
        self.text = text
		self.answerType = answerType
        self.answer = ""
    }
}
class TextQuestion : Question {
    var defaultText: String
    init(id: String, text: String, answerType: AnswerType, defaultText: String = "") {
        self.defaultText = defaultText
		super.init(id: id, text: text, answerType: answerType)
    }
}
class PasswordQuestion : Question {
    var defaultText: String
    init(id: String, text: String, answerType: AnswerType, defaultText: String = "") {
        self.defaultText = defaultText
        super.init(id: id, text: text, answerType: answerType)
    }
}
class NumericQuestion : Question {
    var defaultNumber: Int
    var minValue: Int
    var maxValue: Int
    init(id: String, text: String, answerType: AnswerType, minValue: Int = 0, maxValue: Int = 5, defaultNumber: Int = 0) {
        self.defaultNumber = defaultNumber
        self.maxValue = maxValue
        self.minValue = minValue
        super.init(id: id, text: text, answerType: answerType)
    }
}
class ChoiceQuestion : Question {
    var choices: [String]
    init(id: String, text: String, answerType: AnswerType, choices: [String]) {
        self.choices = choices
        super.init(id: id, text: text, answerType: answerType)
    }
}
