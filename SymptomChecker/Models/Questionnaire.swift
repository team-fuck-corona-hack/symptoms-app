//
//  Questionnaire.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 21.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation

class Questionnaire {
    var questions: [Question]
    var answers: [String: Answer] = [:]
    
    init(questions: [Question]) {
        self.questions = questions
    }
}
