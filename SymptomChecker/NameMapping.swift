//
//  NameMapping.swift
//  SymptomChecker
//
//  Created by Alexander Prams on 22.03.20.
//  Copyright © 2020 Mayflower GmbH. All rights reserved.
//

import Foundation
import SwiftUI

func getDisplayName(inStr: String) -> String{
    let dict = [
        AgeRange.youngerThan40.rawValue:"Jünger als 40",
        AgeRange.between40And50.rawValue:"Zwischen 40 und 50",
        AgeRange.between51And60.rawValue:"Zwischen 51 und 60",
        AgeRange.between61And70.rawValue:"Zwischen 61 und 70",
        AgeRange.between71And80.rawValue:"Zwischen 71 und 80",
        AgeRange.above80.rawValue:"Über 80",
        Gender.male.rawValue:"Männlich",
        Gender.female.rawValue:"Weiblich",
        Gender.other.rawValue:"Divers",
    ]
    
    if dict.keys.contains(inStr) {
        return dict[inStr]!
    } else {
        return inStr
    }
}
